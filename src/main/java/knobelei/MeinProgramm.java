package knobelei;

import java.util.ArrayList;

public class MeinProgramm {

	public static void main(String[] args)
			throws Exception
	{
		final Integer[] alle_zahlen={16,12,18,25,21,11,13,36};
		 
		findeAlleSummenVon(alle_zahlen, 100);
	}
	
	static void findeAlleSummenVon(final Integer[] alle_zahlen, final Integer gesuchte_summe)
	{
		 final Integer[][] alle_gleichungen=erstelleAlleGruppenVon(alle_zahlen);
		 
		 for (Integer zähler=0; zähler<alle_gleichungen.length; zähler=zähler+1)
		 {
				final Integer[] gleichung=alle_gleichungen[zähler];
				final Integer summe=berechneSummeVon(gleichung);
		 
				if (summe==gesuchte_summe)
					zeigeSummeAnVon(gleichung);
		}
	}

	static Integer[][] erstelleAlleGruppenVon(final Integer[] alle_meine_zahlen)
	{
		final Integer anzahl_aller_möglichkeiten=berechneBitVonZähler(alle_meine_zahlen.length)-1;
		Integer[][] möglichkeiten=new Integer[anzahl_aller_möglichkeiten][];
		
		for (Integer bitmuster=0; bitmuster<anzahl_aller_möglichkeiten; bitmuster=bitmuster+1)
		{
			final Integer[] gruppe = erstelleGruppeVon(alle_meine_zahlen, bitmuster);
			möglichkeiten[bitmuster] = gruppe; 
		}
		
		return möglichkeiten;
	}

	static Integer[] erstelleGruppeVon(final Integer[] alle_meine_zahlen, final Integer bitmuster)
	{
	  ArrayList<Integer> zahlen=new ArrayList<Integer>();
	  
	  for (Integer bitzähler=0; bitzähler<alle_meine_zahlen.length; bitzähler=bitzähler+1)
	  {
	  	final Integer zahl=alle_meine_zahlen[bitzähler];
	  	
	  	if (wirdZahlGenommen(bitzähler, bitmuster))
	  		zahlen.add(zahl);	
	  }
	  
	  return zahlen.toArray(new Integer[zahlen.size()]);
	}

	static Integer berechneSummeVon(final Integer[] meine_zahlen)
	{
		Integer meine_summe=0;
		
		for (Integer zähler=0; zähler<meine_zahlen.length; zähler=zähler+1)
		{
	  	final Integer zahl=meine_zahlen[zähler];
			
			meine_summe=meine_summe+zahl;
		}
		
		return meine_summe;
	}

	static void zeigeSummeAnVon(final Integer[] meine_zahlen)
	{
		String text="";
		
		for (Integer zahl=0; zahl<meine_zahlen.length; zahl=zahl+1)
		{
			final String text_zahl=meine_zahlen[zahl].toString();
			
			text=text+text_zahl;
			
			if (zahl+1<meine_zahlen.length)
				text=text+"+";
			else
				text=text+"=";
		}

		final String text_summe=berechneSummeVon(meine_zahlen).toString(); 
		System.out.println(text+text_summe);
	}

	static Integer berechneBitVonZähler(final Integer meine_zahl)
	{
	  return 1<<meine_zahl;
	}
	
	static Boolean wirdZahlGenommen(final Integer mein_bitzähler, final Integer mein_bitmuster)
	{

		final Integer bit=berechneBitVonZähler(mein_bitzähler);
		final Integer bit_gesetzt=bit&mein_bitmuster;
		
		if (bit_gesetzt>0)
			return JA;
		else
			return NEIN;
	}		

	static final Boolean JA = true;
	static final Boolean NEIN = false;
}
